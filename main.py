import os
import subprocess


def format_sizes(bytes):
  if bytes < 1024:
    return f"{bytes} B"
  elif bytes < 1024*1024:
    return f"{bytes/1024:.2f} KiB"
  elif bytes < 1024*1024*1024:
      return f"{bytes/1024/1024:.2f} MiB"
  elif bytes < 1024*1024*1024*1024:
      return f"{bytes/1024/1024/1024:.2f} GiB"
  else:
      return f"{bytes/1024/1024/1024/1024:.2f} TiB"

def format_delta(bytes):
  flip = bytes < 0
  return "-"+format_sizes(-bytes) if flip else format_sizes(bytes)

pool_infos = subprocess.run(["zpool", "list", "-Hp", "-o", "name,size,allocated,freeing"], stdout=subprocess.PIPE).stdout.decode('utf-8')
pool_infos = pool_infos.split("\n")

for pool_info in pool_infos:
    if len(pool_info) < 1:
        continue
    pool_info = pool_info.split()
    name = pool_info[0]
    size = int(pool_info[1])
    alloc = int(pool_info[2])
    freeing = int(pool_info[3])
    compressratio = subprocess.run(["zfs", "get", "-H", "compressratio", name], stdout=subprocess.PIPE).stdout.decode('utf-8').split()[2]
    percent = alloc/size*100
    
    color = "${color white}"
    if percent > 70:
        color = "${color yellow}"
    if percent > 80:
        color = "${color red}"
    alignr = "${alignr}${color}"
    freeing_formatted = "  ${alignc}${color green}" + format_sizes(freeing)
    print(f"{color}{name} {format_sizes(alloc)}/{format_sizes(size)} ({compressratio}){alignr} ({percent:.2f}%)")


arc_size = int(subprocess.run("cat /proc/spl/kstat/zfs/arcstats | grep ^size", stdout=subprocess.PIPE, check=True, shell=True, text=True).stdout.split()[2])

fake_available_ram = int(subprocess.run(["free", "-b"], stdout=subprocess.PIPE).stdout.decode('utf-8').split("\n")[1].split()[6])

true_available_ram = fake_available_ram + arc_size
print(f"ARC Size: {format_sizes(arc_size)}  {alignr}Available memory: {format_sizes(true_available_ram)}")
